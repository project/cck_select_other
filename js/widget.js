/**
 * @file
 * CCK Select Other Javascript Behaviors
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.cckSelectOther = {
    /**
     * Bind to each cck select other field widget delta select element.
     */
    attach: function () {
      var n, i,
          list_element = '';
      var actionBind = this.getActionBind();

      // Prevent errors
      if (typeof drupalSettings.CCKSelectOther !== 'object') {
        return;
      }

      // Assume that JQuery 1.9 works with MSIE because they removed the
      // .browser functionality. JQueryWTF.
      for (n in drupalSettings.CCKSelectOther) {
        for (i in drupalSettings.CCKSelectOther[n]) {
          list_element = $('[data-drupal-selector="edit-' + drupalSettings.CCKSelectOther[n][i] + '-select-other-list"]');
          $(list_element)
            .on(actionBind, null, {
              element: list_element,
              fieldSelector: drupalSettings.CCKSelectOther[n][i]
            }, this.toggle)
            .trigger(actionBind);
        }
      }
    },
    /**
     * Look through selected options of select list, and toggle the display
     * based on whether or not other is selected.
     */
    toggle: function (e) {
      var input_selector = '[data-drupal-selector="edit-' + e.data.fieldSelector + '-select-other-text-input"]';
      var input_element = $(input_selector);
      var selected_other = 'none';

      $(this).children(':selected').each(function() {
          selected_other = ($(this).val() === 'other') ? 'block' : 'none';
      });

      $(input_element).css('display', selected_other);
    },
    /**
     * Provides a backwards-compatible way of supporting Drupal 7 core jQuery,
     * and contemporary versions of jQuery without browser detection support.
     *
     * @returns {string}
     */
    getActionBind: function() {
      var action = 'change';
      if (undefined !== $.browser && $.browser.msie === true) {
        action = 'click';
      }
      return action;
    }
  };
})(jQuery, Drupal, drupalSettings);

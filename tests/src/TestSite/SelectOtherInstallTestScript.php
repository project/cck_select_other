<?php

namespace Drupal\Tests\cck_select_other\TestSite;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\TestSite\TestSetupInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;

/**
 * Setup file used for cck_select_other Nightwatch tests.
 */
class SelectOtherInstallTestScript implements TestSetupInterface {

  /**
   * Random generator.
   *
   * @var \Drupal\Component\Utility\Random|null
   */
  protected $randomGenerator;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected $modules = [
    'node',
    'field',
    'field_ui',
    'options',
    'text',
    'cck_select_other',
  ];

  /**
   * {@inheritdoc}
   */
  public function setup() {
    \Drupal::service('module_installer')->install($this->modules);

    $typeId = $this->createContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);

    $options = $this->createOptions();
    [$default_value] = $this->getRandomOption($options);
    $storage_values = [
      'field_name' => 'field_random_choice',
      'settings' => ['allowed_values' => $options],
      'cardinality' => 1,
    ];
    $config_values = [
      'field_name' => 'field_random_choice',
      'label' => 'Random choice',
      'required' => 0,
      'default_value' => [
        ['value' => $default_value],
      ],
    ];
    $this->createSelectOtherListField('node', $typeId, 'list_string', $storage_values, $config_values);

    // Delete cache.
    \Drupal::cache()->deleteAll();
  }

  /**
   * Install configuration for modules.
   *
   * @param array $values
   *   Optional values to set on the content type.
   *
   * @return string
   *   The node type identifier.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createContentType(array $values = []) {
    if (!isset($values['type'])) {
      $id = $this->randomMachineName();
    }
    else {
      $id = $values['type'];
    }

    $values += [
      'type' => $id,
      'name' => $id,
    ];

    $type = NodeType::create($values);
    $type->save();

    // Create Entity and View displays.
    $formDisplay = EntityFormDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => $id,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $formDisplay->save();

    $viewDisplay = EntityViewDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => $id,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $viewDisplay->save();

    return $id;
  }

  /**
   * Creates a select other field on the content type.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The bundle id.
   * @param string $type
   *   The field type plugin ID.
   * @param array $fieldInfo
   *   The field storage configuration.
   * @param array $instanceInfo
   *   The field configuration.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\field\Entity\FieldStorageConfig
   *   The field config instance.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelectOtherListField($entity_type = 'node', $bundle = 'page', $type = 'list_string', array $fieldInfo = [], array $instanceInfo = []) {
    $random = $this->getRandomGenerator();

    if (!isset($fieldInfo['field_name'])) {
      $fieldInfo['field_name'] = strtolower($random->name(8, TRUE));
    }

    // Create field storage instance.
    $storage_values = NestedArray::mergeDeep($fieldInfo, [
      'entity_type' => $entity_type,
      'type' => $type,
    ]);

    $fieldStorage = FieldStorageConfig::create($storage_values);
    $fieldStorage->save();

    // Create field instance.
    if (!isset($instanceInfo['label'])) {
      $instanceInfo['label'] = $random->string(15);
    }
    $field_values = NestedArray::mergeDeep($instanceInfo, [
      'field_name' => $fieldStorage->getName(),
      'entity_type' => $entity_type,
      'bundle' => $bundle,
    ]);
    $field = FieldConfig::create($field_values);
    $field->save();

    // Create form and display entities for select other field.
    $display_id = $entity_type . '.' . $bundle . '.default';
    $formDisplay = EntityFormDisplay::load($display_id);
    $formDisplay->setComponent($fieldStorage->getName(), [
      'type' => 'cck_select_other',
      'weight' => -1,
    ]);
    $formDisplay->save();

    $viewDisplay = EntityViewDisplay::load($display_id);
    $viewDisplay->setComponent($fieldStorage->getName(), [
      'type' => 'cck_select_other',
      'weight' => -1,
    ]);
    $viewDisplay->save();

    return $fieldStorage;
  }

  /**
   * Generates a unique random string containing letters and numbers.
   *
   * @param int $length
   *   Length of random string to generate.
   *
   * @return string
   *   Randomly generated unique string.
   *
   * @see \Drupal\Component\Utility\Random::name()
   */
  public function randomMachineName($length = 8) {
    return $this->getRandomGenerator()->name($length, TRUE);
  }

  /**
   * Gets the random generator for the utility methods.
   *
   * @return \Drupal\Component\Utility\Random
   *   The random generator
   */
  protected function getRandomGenerator() {
    if (!is_object($this->randomGenerator)) {
      $this->randomGenerator = new Random();
    }
    return $this->randomGenerator;
  }

  /**
   * Create select list options.
   *
   * @param int $num
   *   The number of options to create.
   * @param string $type
   *   The field type.
   *
   * @return array
   *   An associative array of allowed values keyed by value and the label as
   *   the array item value.
   */
  public function createOptions($num = 5, $type = 'list_string') {
    $options = [];
    for ($i = 0; $i < $num; $i++) {
      if ($type === 'list_string') {
        $label = $this->getRandomGenerator()->word(10);
        $key = strtolower($label);
      }
      else {
        $label = $i;
        $key = $i;
      }
      $options[$key] = $label;
    }
    return $options;
  }

  /**
   * Gets a random option in a list of options.
   *
   * @param array $options
   *   An array of list options.
   *
   * @return array
   *   An indexed array of the key and value.
   */
  public function getRandomOption(array $options) {
    $option = array_rand($options);
    return [$option, $options[$option]];
  }

}

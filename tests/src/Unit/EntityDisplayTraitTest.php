<?php

namespace Drupal\Tests\cck_select_other\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the entity display trait.
 *
 * Note that prophecy cannot be used to mock because there is a conflict between
 * the base mockForTrait method and prophecy. PHPUnitWTF.
 *
 * @group cck_select_other
 */
class EntityDisplayTraitTest extends UnitTestCase {

  /**
   * The entity manager mock.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the entity manager.
    $this->entityManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface');

    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $this->entityManager);
    \Drupal::setContainer($container);
  }

  /**
   * Assert that select other widget is detected on form displays.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param bool $expected
   *   The expected result.
   *
   * @dataProvider hasSelectOtherWidgetProvider
   */
  public function testHasSelectOtherWidget($plugin_id, $expected) {
    $mock = $this->getMockForTrait('\Drupal\cck_select_other\EntityDisplayTrait');

    // Mock Field plugin settings.
    $fieldSettings = $this->createMock('\Drupal\Core\Field\PluginSettingsInterface');
    $fieldSettings
      ->expects($this->any())
      ->method('getPluginId')
      ->willReturn($plugin_id);

    // Mock Entity Form Display.
    $entityDisplay = $this->createMock('\Drupal\Core\Entity\Display\EntityDisplayInterface');
    $entityDisplay
      ->expects($this->any())
      ->method('getRenderer')
      ->with('field_list')
      ->willReturn($fieldSettings);

    // Mock the entity storage interface.
    $entityStorage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');
    $entityStorage
      ->expects($this->any())
      ->method('loadByProperties')
      ->with(['targetEntityType' => 'entity_test'])
      ->willReturn(['entity_test.display.mock' => $entityDisplay]);

    // Mock entity manager methods.
    $this->entityManager
      ->expects($this->any())
      ->method('getStorage')
      ->with('entity_form_display')
      ->willReturn($entityStorage);

    // Mock the definition.
    $definition = $this->createMock('\Drupal\Core\Field\FieldDefinitionInterface');
    $definition
      ->expects($this->any())
      ->method('getTargetEntityTypeId')
      ->willReturn('entity_test');
    $definition
      ->expects($this->any())
      ->method('getName')
      ->willReturn('field_list');

    $this->assertEquals($expected, $mock->hasSelectOtherWidget($definition));
  }

  /**
   * Provide parameters for testHasSelectOtherWidget().
   *
   * @return array
   *   An array of test parameters.
   */
  public function hasSelectOtherWidgetProvider() {
    return [
      ['cck_select_other', TRUE],
      ['textfield', FALSE],
    ];
  }

  /**
   * Asserts that select other widget settings are returned or not.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $expected
   *   The expected result.
   *
   * @dataProvider getWidgetSettingsProvider().
   */
  public function testGetWidgetSettings($plugin_id, array $expected) {
    $mock = $this->getMockForTrait('\Drupal\cck_select_other\EntityDisplayTrait');

    // Mock Field plugin settings.
    $fieldSettings = $this->createMock('\Drupal\Core\Field\PluginSettingsInterface');
    $fieldSettings
      ->expects($this->any())
      ->method('getPluginId')
      ->willReturn($plugin_id);
    $fieldSettings
      ->expects($this->any())
      ->method('getSettings')
      ->willReturn($expected);

    // Mock Entity Form Display.
    $entityDisplay = $this->createMock('\Drupal\Core\Entity\Display\EntityDisplayInterface');
    $entityDisplay
      ->expects($this->any())
      ->method('getRenderer')
      ->with('field_list')
      ->willReturn($fieldSettings);

    // Mock the entity storage interface.
    $entityStorage = $this->createMock('\Drupal\Core\Entity\EntityStorageInterface');
    $entityStorage
      ->expects($this->any())
      ->method('loadByProperties')
      ->with(['targetEntityType' => 'entity_test'])
      ->willReturn(['entity_test.display.mock' => $entityDisplay]);

    // Mock entity manager methods.
    $this->entityManager
      ->expects($this->any())
      ->method('getStorage')
      ->with('entity_form_display')
      ->willReturn($entityStorage);

    // Mock the definition.
    $definition = $this->createMock('\Drupal\Core\Field\FieldDefinitionInterface');
    $definition
      ->expects($this->any())
      ->method('getTargetEntityTypeId')
      ->willReturn('entity_test');
    $definition
      ->expects($this->any())
      ->method('getName')
      ->willReturn('field_list');

    $this->assertEquals($expected, $mock->getWidgetSettings($definition));
  }

  /**
   * Get test parameters for testGetWidgetSettings().
   *
   * @return array
   *   Test parameters.
   */
  public function getWidgetSettingsProvider() {
    return [
      ['cck_select_other', ['other_label' => 'Other']],
      ['textfield', []],
    ];
  }

}

module.exports = {
  '@tags': ['cck_select_other'],
  before(browser) {
    const setupFile = `${__dirname}/../../TestSite/SelectOtherInstallTestScript.php`;
    browser.drupalInstall({ setupFile });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Test form display has select other configured': (browser) => {
    browser
      .drupalLoginAsAdmin(() => {
        browser
          .drupalRelativeURL('/admin/structure/types/manage/page/form-display')
          .assert.visible('option[value="cck_select_other"]');
      })
      .drupalLogAndEnd({ onlyOnError: false });
  },
  'Test selecting other makes text input visible': (browser) => {
    browser
      .drupalCreateUser({
        name: 'sam',
        password: 'sam',
        permissions: ['bypass node access'],
      })
      .drupalLogin({ name: 'sam', password: 'sam' })
      .drupalRelativeURL('/node/add')
      .assert.not.visible('.form-select-other-text-input')
      .click('select[name="field_random_choice[0][select_other_list]"] option[value="other"]')
      .waitForElementVisible('input[name="field_random_choice[0][select_other_text_input]"]', 1000)
      .assert.visible('input[name="field_random_choice[0][select_other_text_input]"]')
      .drupalLogAndEnd({ onlyOnError: false });
  },
};

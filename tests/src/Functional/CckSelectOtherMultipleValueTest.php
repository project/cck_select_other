<?php

namespace Drupal\Tests\cck_select_other\Functional;

/**
 * Tests that multiple values are saved properly.
 *
 * @group cck_select_other
 */
class CckSelectOtherMultipleValueTest extends CckSelectOtherTestBase {

  /**
   * Test field validation for required attribute.
   */
  public function testMultipleValues() {
    $options = $this->createOptions();

    $field_info = [
      'settings' => [
        'allowed_values' => $options,
      ],
      'cardinality' => 3,
    ];
    $config_info = ['required' => 0];
    $field = $this->createSelectOtherListField('list_string', $field_info, $config_info);
    $field_name = $field->getName();

    // Login as content creator.
    $this->drupalLogin($this->webUser);

    [$value, $label] = $this->getRandomOption($options);
    $other_value = $this->getRandomGenerator()->word(15);
    $third_value = $this->getRandomGenerator()->word(5);

    $this->drupalGet('/node/add/' . $this->contentType->id());
    $edit = [
      'title[0][value]' => $this->randomString(25),
      $field_name . '[0][select_other_list]' => 'other',
      $field_name . '[0][select_other_text_input]' => $other_value,
      $field_name . '[1][select_other_list]' => $value,
      $field_name . '[1][select_other_text_input]' => '',
      $field_name . '[2][select_other_list]' => 'other',
      $field_name . '[2][select_other_text_input]' => $third_value,
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()
      ->elementTextContains('css', 'div', $options[$value]);
    $this->assertSession()
      ->elementTextContains('css', 'div', $other_value);
    $this->assertSession()
      ->elementTextContains('css', 'div', $third_value);

    $this->drupalGet('/node/1/edit');
    $this->assertOptionElementSelected('edit-' . $field_name . '-0-select-other-list', 'other', 'Select list #0 other selected');
    $this->assertOptionElementSelected('edit-' . $field_name . '-1-select-other-list', $value, 'Select list #1 other selected');
    $this->assertOptionElementSelected('edit-' . $field_name . '-2-select-other-list', 'other', 'Select list #2 other selected');

    $edit = [
      $field_name . '[2][select_other_list]' => '_none',
      $field_name . '[2][select_other_text_input]' => '',
    ];
    $this->submitForm($edit, 'Save');

    $this->assertSession()
      ->elementTextContains('css', 'div', $options[$value]);
    $this->assertSession()
      ->elementTextContains('css', 'div', $other_value);

  }

}

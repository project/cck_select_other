<?php

namespace Drupal\Tests\cck_select_other\Functional;

/**
 * Tests that required fields work correctly.
 *
 * @group cck_select_other
 */
class CckSelectOtherRequiredTest extends CckSelectOtherTestBase {

  /**
   * Asserts required field validation.
   *
   * @param string $expectedMessage
   *   The expected message to look for on the page.
   * @param bool $isValid
   *   TRUE if the test should assert validity.
   * @param int $required
   *   The required value of the field configuration.
   * @param string $list_value
   *   The select other list value to use.
   * @param string $other_value
   *   The select other text field value to use.
   *
   * @dataProvider provideEmptyValues
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRequired($expectedMessage, $isValid, $required, $list_value, $other_value) {
    $options = $this->createOptions();
    $storage_values = [
      'settings' => ['allowed_values' => $options],
      'cardinality' => 1,
    ];
    $config_values = ['required' => $required];

    $field = $this->createSelectOtherListField('list_string', $storage_values, $config_values);
    $field_name = $field->getName();

    // Log in and try to create content with an empty value.
    $this->drupalLogin($this->webUser);

    // Tries selecting an empty value based on test arguments.
    $edit = [
      'title[0][value]' => $this->randomString(25),
      $field_name . '[0][select_other_list]' => $list_value,
      $field_name . '[0][select_other_text_input]' => $other_value,
    ];
    $this->drupalGet('/node/add/' . $this->contentType->id());
    $this->submitForm($edit, 'Save');
    $selector = '//input[@name="' . $field_name . '[0][select_other_text_input]" and contains(@class, "error")]';
    $this->assertSession()->responseContains($expectedMessage);
    if ($isValid) {
      $this->assertSession()->elementNotExists('xpath', $selector);
    }
    else {
      $this->assertSession()->elementExists('xpath', $selector);
      // Successfully post the form.
      $value = $this->getRandomGenerator()->word(25);
      $edit[$field_name . '[0][select_other_list]'] = 'other';
      $edit[$field_name . '[0][select_other_text_input]'] = $value;
      $this->submitForm($edit, 'Save');
      $this->assertSession()->pageTextContains($value);
    }
  }

  /**
   * Provides test arguments to test empty values.
   *
   * @return array
   *   An array of test arguments.
   */
  public function provideEmptyValues() {
    return [
      'required other value is empty' => [
        'You must provide a value for this option.',
        FALSE,
        1,
        'other',
        '',
      ],
      'required list value is _none' => [
        'The value you selected is not a valid choice.',
        FALSE,
        1,
        '_none',
        '',
      ],
      'optional list value is _none' => [
        'has been created.',
        TRUE,
        0,
        '_none',
        '',
      ],
    ];
  }

}
